import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.net.*;
import java.util.Scanner;

public class GetNameMain {
    public static void main(String[] args) {
        String url = "https://reqres.in/api/users/";
//        int userId = getIdFromConsole();
        int userId = Integer.parseInt(args[0]);

        JSONObject data = getDataFromUrlById(url, userId);

        if (data != null) {
            String firstName = (String) data.get("first_name");
            String lastName = (String) data.get("last_name");
            System.out.printf("%s %s", firstName, lastName);
        }
    }

    public static int getIdFromConsole() {
        System.out.println("Enter user id: ");
        Scanner scanner = new Scanner(System.in);
        int userId = scanner.nextInt();
        return userId;
    }

    public static JSONObject getDataFromUrlById(String url, int userId) {
        try {
            URLConnection connection = new URL(url + userId).openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            Object obj = new JSONParser().parse(in.readLine());
            JSONObject jo = (JSONObject) obj;
            JSONObject data = (JSONObject) jo.get("data");
            return data;
        } catch (IOException e) {
            System.out.println("User not found!");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
